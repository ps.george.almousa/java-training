public class Matrix {
    //Q1
    public int[][] sumArray(int[][] arr1, int[][] arr2) {
        int[][] result = new int[arr1.length][arr1[0].length];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[0].length; j++) {
                result[i][j] = arr1[i][j] + arr2[i][j];
                System.out.println(result[i][j] + " ");
            }
        }
        return result;
    }

    //Q2
    public int[][] multiScalar(int[][] arr1) {
        int number = 2;
        int[][] result = new int[arr1.length][arr1[0].length];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[0].length; j++) {
                result[i][j] = number * arr1[i][j];
                System.out.println(result[i][j]);
            }
        }
        return result;
    }

    //Q3
    public int[][] transArray(int[][] arr1) {
        int[][] result = new int[arr1[0].length][arr1.length];
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr1[0].length; j++) {
                result[j][i] = arr1[i][j];
            }
        }
        return result;
    }
    // Q4
    public int [][] multiTwoArray(int [][] arr1, int [][]arr2){
        int [][] result=new int[arr1[0].length][arr2.length];
        for(int i=0; i<arr1.length;i++){
            for(int j =0; j < arr1[0].length;j++){
                for(int k=0; k <arr2[0].length; k++) {
                    result[i][j] += arr1[i][k]*arr2[k][j];
                }
            }
        }
        return result;
    }
    //Q5
    public int [][] subMatrix(int [][] arr1,int numOfDeletedRows,int numberOfDeleteColumns){
        int [][] result=new int[arr1.length-1] [arr1[0].length-1];
        for(int i=0,row=0;i<arr1.length;i++){
            if(i==numOfDeletedRows-1) continue;
            for(int j=0,col=0;j<arr1[0].length;j++) {
                if (j == numberOfDeleteColumns-1) continue;
                result[row][col] = arr1[i][j];
                col++;
            }
            row++;
        }
        return result;
    }
    //Q6 part 1
    public int[][] diagonalArray(int[][] arr) {
        int[][] result = new int[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                if (i == j) {
                    result[i][j] = arr[i][j];
                } else {
                    result[i][j] = 0;
                }
            }
        }
        return result;
    }

    //Q6 part 2
    public int[][] lowerArray(int[][] arr) {
        int[][] result = new int[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                if (i >= j) {
                    result[i][j] = arr[i][j];
                } else {
                    result[i][j] = 0;
                }
            }
        }
        return result;
    }
// Q6 part 3

    public int[][] upperArray(int[][] arr) {
        int[][] result = new int[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                if (i <= j) {
                    result[i][j] = arr[i][j];
                } else {
                    result[i][j] = 0;
                }
            }
        }
        return result;
    }

    public double sequare(int [][] arr){
        int N =arr.length;
        double result = 0;
        if(N==1){
          return arr[0][0];
        }
        for (int i = 0;i < N;i++){
             result += Math.pow(-1,i) * arr[0][i] * sequare(subMatrix(arr,1,i+1)); // here we got the constant of the array
        }
        return result;
    }

}

